<?php
/**
 * browscap-php(https://github.com/browscap/browscap-php)
 * をじわっと拡張
 *
 * @author fujii@sharecoto.co.jp
 */

use phpbrowscap\Browscap;

class Sharecoto_Browscap extends Browscap
{

    protected $currentBrowser;

    public function __construct($cache_dir = '/tmp', $ua=null)
    {
        $this->doAutoUpdate = false;
        parent::__construct($cache_dir);

        if ($ua === null) {
            $ua = $_SERVER['HTTP_USER_AGENT'];
        }

        $this->currentBrowser = $this->getBrowser($ua);
    }

    public function isMobile()
    {
        if (null === $this->currentBrowser) {
            $this->currentBrowser = $this->getBrowser();
        }

        return $this->currentBrowser->isMobileDevice;
    }

    public function getBrowser($user_agent = null, $return_array = false)
    {
        if ($user_agent === null && $return_array == false && $this->currentBrowser) {
            return $this->currentBrowser;
        }

        return parent::getBrowser($user_agent, $return_array);
    }

    public function isLtIe8()
    {
        return $this->isLtIe(8);
    }

    /**
     * $varで指定されたメジャーバージョンよりも下ならtrue
     *
     * @param integer $var
     * @return boolean
     */
    public function isLtIe($var)
    {
        if (null === $this->currentBrowser) {
            $this->currentBrowser = $this->getBrowser();
        }

        if ($this->currentBrowser->MajorVer <= $var && $this->currentBrowser->Browser === 'IE') {
            return true;
        }

        return false;
    }
}
