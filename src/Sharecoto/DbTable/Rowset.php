<?php
class Sharecoto_DbTable_Rowset extends Zend_Db_Table_Rowset
{
    /**
     * Zend_Db_Table_Row_Abstract class name.
     *
     * @var string
     */
    protected $_rowClass = 'Sharecoto_DbTable_Row';
}
