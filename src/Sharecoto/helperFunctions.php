<?php
/**
 * Zend Frameworkのくどさを緩和する各種関数
 *
 * @author fujii@sharecoto.co.jp
 */

/**
 * Zend_View_Helper_BaseUrlのラッパ
 *
 * @param string|null $file
 */
function _b($file, $hash = true)
{
    $helper = Sharecoto_Helper::getInstance();

    if (_getRevision() && $hash) {
        @list($path, $q) = explode('?', $file);
        $query = array('_rev_' => _getRevision());
        if ($q) {
            $query = array_merge(
                $query,
                parse_str($q)
            );
        }
        $file = $path . '?' . http_build_query($query);
    }

    return $helper->getView()->baseUrl($file);
}

/**
 * Zend_View_::escapeのラッパ
 *
 * @return string
 */
function _e($str)
{
    $helper = Sharecoto_Helper::getInstance();
    return $helper->getView()->escape($str);
}

/**
 * Zend_View::escapeにnl2br()
 *
 * @return string
 */
function _ebr($str)
{
    return nl2br(_e($str));
}

/**
 * コンフィグのうち、appで始まるのを返す
 *
 * @return Zend_Config
 */
function _getAppConfig()
{
    $helper = Sharecoto_Helper::getInstance();
    return $helper->getAppConfig();
}

/**
 * JS圧縮
 *
 * @return string javascript
 */
function _jsPack($code)
{
    $pack = new Sharecoto_JsPack($code);
    return $pack->pack(true);
}

/**
 * 現在のgitのリビジョンを返す
 * git コマンドがエラーになったらfalse
 *
 * @return string|bool
 */
function _getRevision()
{
    if (defined('VCS_REVISION')) {
        return VCS_REVISION;
    }

    $rev = exec('git rev-parse --verify HEAD 2>/dev/null', $output, $returnCode);
    if ($returnCode === 0) {
        define('VCS_REVISION', substr($rev, 0, 8));
    } else {
        define('VCS_REVISION', false);
    }

    return VCS_REVISION;
}

/**
 * HTMLPurifierのオブジェクトを返す
 *
 * @param array $config http://htmlpurifier.org/live/configdoc/plain.html
 * @return HTMLPurifier
 */
function _getPurifier(array $config = array())
{
    $config = HTMLPurifier_Config::createDefault();

    $cacheDir = '/tmp/purifier';
    if ((file_exists($cacheDir) && !is_writable($cacheDir)) || (file_exists($cacheDir) && !is_dir($cacheDir))) {
        $cacheDir .= '_' . _getRevision();
    }
    if (!file_exists($cacheDir)) {
        mkdir($cacheDir);
    }

    $config->loadArray(
        array(
            'AutoFormat.Linkify' => true,
            'Core.Encoding' => 'UTF-8',
            'Cache.SerializerPath' => $cacheDir
        )
    );

    return new HTMLPurifier($config);
}

/**
 * YoutubeのURLから動画のIDを抜き出す
 *
 * @params string $url YoutubeのUR
 * @return string|null
 */
function _getYoutubeId($url)
{
    if (strpos($url, 'youtube.com') === false) {
        return null;
    }

    $parsedUrl = parse_url($url);
    // embedのとき
    if (strpos($url, '/embed/') !== false) {
        $regExp = '#/embed/([\d\w]+)#';
        preg_match($regExp, $parsedUrl['path'], $match);
        $youtubeId = $match[1];
        return $youtubeId;
    } elseif (strpos($url, '/v/') !== false) {
        $regExp = '#/v/([\d\w]+)#';
        preg_match($regExp, $parsedUrl['path'], $match);
        $youtubeId = $match[1];
        return $youtubeId;
    }

    parse_str($parsedUrl['query'], $query);
    $youtubeId = $query['v'];
    return $youtubeId;
}
