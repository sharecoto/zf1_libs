<?php

class PluginDeviceTest extends PHPUnit_Framework_TestCase
{
    public function testInstance()
    {
        $dv = new Plugin_Device(
            array(
                'HTTP_USER_AGENT' => ''
            )
        );
    }

    public function testDetectCareerPC()
    {
        $dv = new Plugin_Device(
            array(
                'HTTP_USER_AGENT' => ''
            )
        );
        $career = $dv->detectCareer();

        $this->assertEquals('PC', $career);
    }

    public function testDetectDevicePC()
    {
        $dv = new Plugin_Device(
            array(
                'HTTP_USER_AGENT' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.52 Safari/537.36'
            )
        );
        $career = $dv->detectCareer();
        $device = $dv->detectDevice($career);

        $this->assertEquals(Plugin_Device::DEVICE_PC, $device);
    }

    public function testDetectCareerAndroidMobile()
    {
        $dv = new Plugin_Device(
            array(
                'HTTP_USER_AGENT' => 'Mozilla/5.0 (Linux; U; Android 2.3.3; ja-jp; SonyEricssonX10i Build/3.0.1.G.0.75) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1'
            )
        );
        $career = $dv->detectCareer();

        $this->assertEquals('AndroidMobile', $career);
    }

    public function testDetectDeviceAndroidMobile()
    {
        $dv = new Plugin_Device(
            array(
                'HTTP_USER_AGENT' => 'Mozilla/5.0 (Linux; U; Android 2.3.3; ja-jp; SonyEricssonX10i Build/3.0.1.G.0.75) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1'
            )
        );
        $career = $dv->detectCareer();
        $device = $dv->detectDevice($career);

        $this->assertEquals(Plugin_Device::DEVICE_SMARTPHONE, $device);
    }

    public function testDetectCareerIphone()
    {
        $dv = new Plugin_Device(
            array(
                'HTTP_USER_AGENT' => 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/9A334 Safari/7534.48.3'
            )
        );
        $career = $dv->detectCareer();

        $this->assertEquals('iPhone', $career);
    }

    public function testDetectDeviceIphone()
    {
        $dv = new Plugin_Device(
            array(
                'HTTP_USER_AGENT' => 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/9A334 Safari/7534.48.3'
            )
        );
        $career = $dv->detectCareer();
        $device = $dv->detectDevice($career);

        $this->assertEquals(Plugin_Device::DEVICE_SMARTPHONE, $device);
    }

    public function testDetectCareerTablet()
    {
        $dv = new Plugin_Device(
            array(
                'HTTP_USER_AGENT' => 'Mozilla/5.0 (iPad; CPU OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/7534.48.3'
            )
        );
        $career = $dv->detectCareer();

        $this->assertEquals('iPad', $career);

        $dv = new Plugin_Device(
            array(
                'HTTP_USER_AGENT' => 'Mozilla/5.0 (Linux; U; Android 4.0.3; ja-jp; Sony Tablet S Build/TISU0R0110) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30'
            )
        );
        $career = $dv->detectCareer();

        $this->assertEquals('Android', $career);
    }

    public function testDetectDeviceTablet()
    {
        $dv = new Plugin_Device(
            array(
                'HTTP_USER_AGENT' => ''
            )
        );

        $career = 'Android';
        $device = $dv->detectDevice($career);
        $this->assertEquals(Plugin_Device::DEVICE_TABLET, $device);

        $career = 'iPad';
        $device = $dv->detectDevice($career);
        $this->assertEquals(Plugin_Device::DEVICE_TABLET, $device);
    }

    public function testDispatchLoopStartUp()
    {
        $mock = Mockery::mock('\Zend_Controller_Request_Abstract');
        $dv = new Plugin_Device(
            array(
                'HTTP_USER_AGENT' => ''
            )
        );
        $dv->dispatchLoopStartup($mock);

        $this->assertTrue(defined('USER_DEVICE_CAREER'));
        $this->assertTrue(defined('USER_DEVICE'));
    }
}
