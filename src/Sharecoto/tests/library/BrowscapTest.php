<?php
class BrowscapTest extends PHPUnit_Framework_TestCase
{
    public function testIsLtIe8()
    {
        $ua = 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)';
        $bcap = new Sharecoto_Browscap('/tmp', $ua);
        $result = $bcap->isLtIe8();
        $this->assertTrue($result);

        $ua = 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)';
        $bcap = new Sharecoto_Browscap('/tmp', $ua);
        $result = $bcap->isLtIe8();
        $this->assertFalse($result);

        $ua = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.69 Safari/537.36';
        $bcap = new Sharecoto_Browscap('/tmp', $ua);
        $result = $bcap->isLtIe8();
        $this->assertFalse($result);
    }

    public function testIsLtIe()
    {
        $ua = 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)';
        $bcap = new Sharecoto_Browscap('/tmp', $ua);
        $result = $bcap->isLtIe(8);
        $this->assertTrue($result);
        $result = $bcap->isLtIe(7);
        $this->assertFalse($result);

    }
}
