<?php
/**
 * Sharecoto\Helperは基本的にテストされているので、
 * 関数コール時に余計なエラーが出ないことだけ簡単に確認します
 */

class HelperFunctionsTest extends PHPUnit_Framework_TestCase
{
    public function testBaseUrl()
    {
        _b('test/test');
    }

    public function testEscape()
    {
        _e('<>');
    }

    public function testEscapeAndNl2br()
    {
        $str = "<\n>";
        $this->assertEquals("&lt;<br />\n&gt;", _ebr($str));
    }

    public function testGetRevision()
    {
        $rev = _getRevision();
    }

    public function testGetYoutubeId()
    {
        $url = 'http://www.youtube.com/embed/TiVHWuzd5Fw';
        $id = 'TiVHWuzd5Fw';

        $youtubeId = _getYoutubeId($url);
        $this->assertEquals($youtubeId, $id);

        $url = 'http://www.youtube.com/v/TiVHWuzd5Fw?version=3&autohide=1&autoplay=1';
        $youtubeId = _getYoutubeId($url);
        $this->assertEquals($youtubeId, $id);

        $url = 'http://www.youtube.com/watch?v=TiVHWuzd5Fw&feature=player_embedded';
        $youtubeId = _getYoutubeId($url);
        $this->assertEquals($youtubeId, $id);
    }
}

