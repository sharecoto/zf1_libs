<?php

class LoggerTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->log = Sharecoto_Logger::getInstance(
            array('HTTP_USER_AGENT' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.52 Safari/537.36')
        );
    }

    public function testInstance()
    {
        $log = Sharecoto_Logger::getInstance();
        $this->assertInstanceOf('Sharecoto_Logger', $log);
    }

    public function testGetConfig()
    {
        $config = $this->log->getConfig();

        $this->assertInstanceOf('Zend_Config', $config);
    }

    public function testGetLogger()
    {
        $logger = $this->log->getLogger();
        $this->assertInstanceOf('Zend_Log', $logger);
    }

    public function testLoggingInfo()
    {
        $this->log->addInfo('info log message');
        $events = $this->log->getWriter()->events;
        $event = end($events);
        $this->assertEquals('INFO', $event['priorityName']);
    }

    public function testLoggingWarn()
    {
        $this->log->addWarn('hogehoge');
        $events = $this->log->getWriter()->events;
        $event = end($events);
        $this->assertArrayHasKey('agent', $event);
        $this->assertArrayHasKey('timestamp', $event);
        $this->assertEquals('WARN', $event['priorityName']);
    }

    public function testLogMessageIsJson()
    {
        $this->log->addNotice(array('test'=>'message'));
        $events = $this->log->getWriter()->events;
        $event = end($events);

        $decode = json_decode($event['message']);
        $this->assertNotNull($decode);
    }

    /**
     * @expectedException Sharecoto_Logger_Exception
     */
    public function testLoggingLebelIsNotExist()
    {
        $this->log->addHoge('hogehoge');
    }

    /**
     * @expectedException Sharecoto_Logger_Exception
     */
    public function testCallUndefinedMethod()
    {
        $this->log->undefinedMethod('hogehoge');
    }

}
