<?php

class AdminTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->admin = new Application_Model_Admin;
    }

    public function testInstanceExtendsBaseModel()
    {
        $this->assertInstanceOf('Application_Model_Base', $this->admin);
    }

    public function testFindByName()
    {
        $name = 'admin';
        $row = $this->admin->findByName($name)->current();

        $this->assertEquals($row->name, $name);
    }
}
